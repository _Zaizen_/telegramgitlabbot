import { bot } from "./bot.ts";
import { env, run, webhookCallback } from "./deps.ts";
import { server } from "./server.ts";
import { router } from "./routes.ts";
import {log} from "./log.ts"
import i18n  from "./i18n.ts"

const t = i18n(env["DEFAULT_LANGUAGE"]);

log.info(t("initializedAs", {name: bot.botInfo.username}))

if (env["USE_WEBHOOK"] == "true") {
  log.info(t("usingWebhook"))

  router.post(
    `${env["TELEGRAM_HOOK_PATH"]}/${env["BOT_TOKEN"]}`,
    webhookCallback(bot, "oak"),
  );
  await bot.api.setWebhook(
    `${env["HOOK_URL"]}${env["TELEGRAM_HOOK_PATH"]}/${env["BOT_TOKEN"]}`,
  );
} else if (env["USE_WEBHOOK"] == "false") {
  log.info(t("notUsingWebhook"))

  const runner = run(bot, 300, {});
  const stopRunner = () => runner.isRunning() && runner.stop();

  Deno.addSignalListener("SIGINT", stopRunner);
  Deno.addSignalListener("SIGTERM", stopRunner);
}

await server.listen({ port: parseInt(env["PORT"]!) || 3000 });
