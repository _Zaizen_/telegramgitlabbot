export const mergeHandler = (body) => {
    if (!body.object_attributes || !body.object_attributes.action || body.changes.labels){
        return ""
    }
    
    return `<b>${body.user.name}</b> ${body.object_attributes.action} 
MR 
<a href="${body.object_attributes.url}">${body.object_attributes.title}</a> 
on 
<a href="${body.project.homepage}">${body.project.name}</a>`

}