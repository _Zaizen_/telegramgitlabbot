export const noteHandler = (body) => {
    /*
    commit
    merge_request
    issue
    snippet
    */

    let url = ""
    let title = ""

    // if body.commit set the url to body.commit.url
    if (body.commit) {
        url = body.commit.url
        title = body.commit.message
    }
    if (body.merge_request) {
        url = body.merge_request.url
        title = body.merge_request.title
    }
    if (body.issue) {
        url = body.issue.url
        title = body.issue.title
    }
    if (body.snippet) {
        url = body.snippet.url
        title = body.snippet.title
    }
    
    return `<b>${body.user.name}</b> commented
<a href="${body.object_attributes.url}">${body.object_attributes.description}</a>
on 
[<a href="${body.project.homepage}">${body.project.name}</a>] <a href="${url}">${title}</a>
`
}