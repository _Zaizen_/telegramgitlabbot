export const pushHandler = (body) => {
  const branch_name = body.ref.split("/").pop();
  let result = "";
  if (body?.commits.length > 0) {
    for (const commit of body.commits) {
      result += "<b>" + commit.author.name + "</b>: ";
      result += '<a href="' + commit.url + '">' + commit.message.trim() +
        '</a> to <a href="';
      result += body.project.homepage + '">' + body.project.name + ":" +
        branch_name + "</a>\n";
    }
  } else {
    if (body.before == "0000000000000000000000000000000000000000") {
      result = "<b>" + body.user_name + "</b>: created branch " +
        '<a href="' + body.project.web_url + "/-/tree/" +
        body.ref.split("/").pop() + '">' + body.project.name + ":" +
        body.ref.split("/").pop() + "</a>";
    } else {
      result = "<b>" + body.user_name + "</b>: deleted branch " +
        '<a href="' + body.project.web_url + "/-/tree/" +
        body.ref.split("/").pop() + '">' + body.project.name + ":" +
        body.ref.split("/").pop() + "</a>";
    }
  }
  return result;
};
