import { truncateString } from "../utils.ts";

export const issueHandler = (body) => {
  if (
    !body.object_attributes ||
    !body.object_attributes.action ||
    body.changes.labels ||
    body.changes.milestone_id
  ) {
    return "";
  }

  switch (body.object_attributes.action) {
    case "open":
      return `<b>${body.user.name}</b> ${body.object_attributes.action} 
issue 
<a href="${body.object_attributes.url}">${body.object_attributes.title}</a> 
on 
<a href="${body.project.homepage}">${body.project.name}</a>
wih description
${truncateString(body.object_attributes.description, 100)}
`;
    case "update":
    case "close":
      return `<b>${body.user.name}</b> ${body.object_attributes.action} 
issue 
<a href="${body.object_attributes.url}">${body.object_attributes.title}</a> 
on 
<a href="${body.project.homepage}">${body.project.name}</a>
`;
    default:
      return "";
  }

};
