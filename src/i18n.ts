import { i18next, Backend } from "./deps.ts";

const systemLocale = Intl.DateTimeFormat().resolvedOptions().locale;

i18next
    .use(Backend)
    .init({
        //debug: true,
        //initImmediate: false,
        fallbackLng: "en",
        preload: ['en', 'it'],
        backend: {
            loadPath: "locales/{{lng}}/{{ns}}.json",
        },
    });

export default (lng: string | undefined | null) =>
    i18next.getFixedT(lng || systemLocale);