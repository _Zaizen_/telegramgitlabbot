import { bot } from "./bot.ts"
import { Menu, Context } from "./deps.ts"

export const start_menu = new Menu("start-menu")
    .text("➕ Aggiungimi ad un gruppo ➕", (ctx: Context) => ctx.reply("Forward!")).row()
    .submenu("ℹ️ Informazioni", "info-menu", (ctx: Context) => ctx.editMessageText("It is " + new Date().toLocaleString()),)
    .submenu("Comandi 🤖", "command-menu").row()

const info_menu = new Menu("info-menu")
    .back("Go Back");

start_menu.register(info_menu);

const command_menu = new Menu("command-menu")
    .back("Go Back");

start_menu.register(command_menu);

export const settings_menu = new Menu("settings-menu")
    .text("🔧 Preferenze 🔧", (ctx: Context) => ctx.reply("Preferences")).row()
    .text("🇮🇹 Languages 🇮🇹", (ctx: Context) => ctx.reply("Backwards!"));
