function truncateString(inputString: string, numChars: number): string {
    // Check if the inputString is shorter than numChars
    if (inputString.length <= numChars) {
      return inputString;
    }
  
    // Find the last space before numChars
    const lastSpaceIndex = inputString.lastIndexOf(" ", numChars);
  
    // If no space is found, just return the inputString truncated at numChars
    if (lastSpaceIndex === -1) {
      return inputString.slice(0, numChars) + "...";
    }
  
    // Otherwise, return the inputString truncated at the last space before numChars
    return inputString.slice(0, lastSpaceIndex) + "...";
  }
  
  export {truncateString}