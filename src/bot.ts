import { Bot, env, Context, hydrateReply, autoRetry, ParseModeFlavor, parseMode, Menu } from './deps.ts'
import { log } from "./log.ts"
import {start_menu, settings_menu} from "./menu.ts"

log.info("Token: " + env["BOT_TOKEN"])

export const bot = new Bot<ParseModeFlavor<Context>>(env["BOT_TOKEN"]);

//bot.api.config.use(autoRetry());
bot.use(hydrateReply);
bot.api.config.use(parseMode("HTML"));

bot.use(start_menu)

bot.command("start", (ctx: Context) => {
    let message = `Benvenuto ${ctx.from?.first_name ?? ""}`
    ctx.reply(message, {
        reply_markup: start_menu,
        reply_to_message_id: ctx.msg.message_id
    });
})

bot.command("hook", (ctx: Context) => {
    let hook_url = env["HOOK_URL"] + env["HOOK_PATH"] + "/"
    const id = parseInt(ctx.msg.chat.id)

    hook_url += ((id < 0) ? ('n' + (-id)) : id)

    ctx.reply("Aggiungi questo agli unciniragnatela: " + hook_url, {
        reply_to_message_id: ctx.msg.message_id
    });
})

bot.use(settings_menu)

bot.command("settings", async (ctx: Context) => {
    await ctx.reply("Qui puoi modificare le impostazioni del bot", { reply_markup: settings_menu, reply_to_message_id: ctx.msg.message_id });
})

await bot.init()