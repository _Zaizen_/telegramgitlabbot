
import {Application, env} from './deps.ts'
import {router} from './routes.ts'
import {log} from "./log.ts"

export const server =  new Application() // The Oak server
server.use(router.routes()); // The routes to handle
server.use(router.allowedMethods()); // Allow all methods, like GET, POST, PUT, DELETE.
server.addEventListener('listen', () => {
    log.info(`Listening on: ${env["PORT"]}`);
})
