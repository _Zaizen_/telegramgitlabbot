import { bot } from './bot.ts';
import {Router, env, RouterContext} from './deps.ts'
import { pushHandler, issueHandler, mergeHandler, noteHandler} from './handlers/handlers.ts';
import {log} from './log.ts';

export const router = new Router()

// Handle post requests for gitlab updates
router.post(env["HOOK_PATH"] + "/:id", async (ctx: RouterContext) => {
    const body = await ctx.request.body({ type: "json" }).value
    
    if (ctx?.params?.id && body?.object_kind){
        // Obtain the chat id from the request url. If the actual chat id is negative there will be a 'n' at the beginning of a positive id
        const id: number = (ctx.params.id[0] == "n") ? (- parseInt(ctx.params.id.substring(1))) : parseInt(ctx.params.id)

        const object_kind = body.object_kind

        // The resulting message that the bot will send
        let result = ""

        log.debug("Tipo richiesta: " + object_kind)
        log.debug("Richiesta: " + JSON.stringify(body))

        switch(object_kind){
            case "push":
                result = pushHandler(body)
                break
            case "merge_request":
                result = mergeHandler(body)
                break
            case "issue":
                result = issueHandler(body)
                break
            case "note":
                result = noteHandler(body)
                break
            default:
                break
        }

        if (result != "") {
            // If the result message is empty this will not send a message
             bot.api.sendMessage(id, result)
        }
        
        // Tell Gitlab that everything went fine
        ctx.response.status = 200;
        ctx.response.body = 'OK';
    }else{
        ctx.response.status=500;
        ctx.response.body = 'Error'
    }
    
})



