import { winston } from './deps.ts'

const formatterConsole = winston.format.printf((info: winston.log) => {
  return `${info.timestamp} [${info.level}]: ${info.message}`;
});

const formatterFile = winston.format.printf((info: winston.log) => {
  return `${info.timestamp}: ${info.message}`;
});

const logging = winston.createLogger({
  level: "debug",
  format: winston.format.combine(
    winston.format((info: winston.log) => {
      info.level = info.level.toUpperCase()
      return info;
    })(),
    winston.format.timestamp({
      format: () => {
        return new Date().getTime()
      }
    }),
    formatterFile
  ),
  timestamp: function () { return (new Date()).getTime(); },
  transports: [
    new winston.transports.File({ filename: "logs/debug.log", level: "debug" }),
    new winston.transports.File({ filename: "logs/info.log", level: "info" }),
    new winston.transports.File({ filename: "logs/warn.log", level: "warn" }),
    new winston.transports.File({ filename: "logs/error.log", level: "error" }),
    new winston.transports.File({ filename: "logs/critical.log", level: "critical" }),
    new winston.transports.File({
      filename: "logs/combined.log",
      format: winston.format.combine(
        winston.format.timestamp({
          format: () => {
            return new Date().getTime()
          }
        }),
        formatterConsole
      ),
    }),
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),

        winston.format.timestamp({
          format: () => {
            return new Date().getTime()
          }
        }),
        formatterConsole
      ),
    })
  ],
});

export let log: winston.logger = logging;