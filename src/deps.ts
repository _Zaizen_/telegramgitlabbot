export {
    Bot,
    Composer,
    type Context,
    InlineKeyboard,
    InputFile,
    session,
    type SessionFlavor,
    webhookCallback,
} from 'grammy';
export {
    type Conversation,
    type ConversationFlavor,
    conversations,
    createConversation,
} from 'grammy_conversations';
export { Menu, MenuRange } from 'grammy_menu';
export {
    hydrateReply,
    parseMode,
    type ParseModeFlavor,
} from 'grammy_parse_mode';
export { run } from 'grammy_runner';
export { autoRetry } from 'grammy_auto_retry';
export {
    Application,
    Router,
    type RouterContext,
} from 'oak';

import winston from "winston"; 

import i18next from "i18next";

import Backend from "i18next_fs_backend";

import { config } from "dotenv";

const env = await config();

export {env, winston, i18next, Backend};