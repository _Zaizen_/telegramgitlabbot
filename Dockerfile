FROM denoland/deno:latest as base
WORKDIR /app

COPY . ./


CMD ["run", "--allow-net", "--allow-read", "--allow-write", "--allow-env", "--import-map", "import_map.json", "./src/main.ts"]



